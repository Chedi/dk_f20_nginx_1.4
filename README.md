dk_f20_nginx_1.4
================

Docker container for nginx 1.4 based on fedora 20


##Info:
This project creates a container running a nginx 1.4.4 service on port 80 and 443 as proxy for a gunicorn process on port 8080  based on the fedora 20 distro

* expose port 22
* expose port 80
* expose port 443


##Usage:

You can rebuild the container by yourself using :

```
docker build git://github.com/Chedi/dk_f20_nginx_1.4.git
```

or directly download it from the docker index

```
docker pull chedi/nginx-1.4
```

once the image ready you can run it with something like this (see my [container_fall repo](https://github.com/Chedi/container_fall)):

```

```


##Meta:
Built with docker-io 0.7.6 on Centos 6.4
